package prøvePackage;

public class Person {

	private String name;
	private boolean isGod;
	
	public Person(String name, boolean isGod) {
		this.setName(name);
		this.setGod(isGod);
	}
	
	public void setGod() {
		this.isGod = true;
	}

	public boolean isGod() {
		return isGod;
	}

	public void setGod(boolean isGod) {
		this.isGod = isGod;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
